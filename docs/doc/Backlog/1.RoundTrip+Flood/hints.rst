.. (C) ALbert Mietus, Sogeti, 2019

Hints
=====

An basic, earlier versions is made and documented at https://messtechpush-training.readthedocs.io/en/latest/index.html

For now, that can be used as a reference.

Todo
----
.. todo:: Import the ``MessTechPush`` variants of *RoundTrip+Flood*

   * Convert the repro should be converted from hg to git
   * git-rewrite to only keep the useful stuff (doc&code about DDS)
   * import into thi git, keeping the history

.. todo:: Import other *RoundTrip+Flood* variant (not/hardy in VCS)

   * See above

.. todo:: SetUp the M&B comparison between DDS and other protocols in this environment (see ``MessTechPush``)

   .. tip:: Is this part of “DDS-Demonstrators”, or another ``HighTech-NL TechPush`` project?


More links
----------
* About the `setup with some raspberry’s
  <https://messtechpush-training.readthedocs.io/en/latest/teams/2018.1_Talk_like_pi/setup.html>`_

* Some roundtrip/flood `sequence diagrams
  <https://messtechpush-training.readthedocs.io/en/latest/teams/2018.1_Talk_like_pi/RoundTrip.html>`_
