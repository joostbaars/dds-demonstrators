.. _TeamPages:

==========
Team pages
==========
:author: Albert
:date: Dec 2019

.. sidebar:: Concept

   Each team (possible of 1 person) gets an *own* directory (a subdir of this one), to place notes, store
   know-how, etc. That does not imply they are the only ones who may change those articles; it just
   a convenient place to start.

   So whenever you find out something, that may be interesting for your team, or a future team:
   write a note, an article, and place it in your team-section. Or update an existing one (even of
   another team). In all cases, add you name as author or section-author and update the date


This part consists of a lot of ‘notes’. They may be new, maintained, or outdated. Moreover, general quality rules are
lowered. This is **on purpose**: it is better to have and share a brainwave, then to have it forgotten. As long a the
reader knows it status: by this intro, everybody should know!

.. toctree::
   :glob:
   :maxdepth: 2

   */index
